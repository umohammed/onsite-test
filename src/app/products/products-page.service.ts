import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { map, shareReplay, switchMap } from 'rxjs/operators';
import { ApiService } from '../data/api.service';

@Injectable()
export class ProductsPageService {
  private selectedProductCategoryIdSubject = new Subject<number>();

  selectedProductCategoryId$ =
    this.selectedProductCategoryIdSubject.asObservable();

  private products$ = this.apiService.products$.pipe(shareReplay(1));

  filteredProducts$ = this.selectedProductCategoryId$.pipe(
    switchMap((id) =>
      this.products$.pipe(
        map((products) =>
          products.filter((product) => product.productCategoryId === id)
        )
      )
    )
  );

  productCategories$ = this.apiService.productCategories$;

  constructor(private apiService: ApiService) {}

  productCategorySelected(id: number) {
    this.selectedProductCategoryIdSubject.next(id);
  }
}
