import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductCategoriesListComponent } from './product-categories-list.component';
import { ProductsPageComponent } from './products-page.component';
import { MatTableModule } from '@angular/material/table';
import { ProductsTableComponent } from './products-table.component';
import { MatListModule } from '@angular/material/list';
import { ShellComponent } from '../core/shell.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: ShellComponent,
    children: [
      {
        path: '',
        component: ProductsPageComponent,
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    MatTableModule,
    MatListModule,
    SharedModule,
  ],
  declarations: [
    ProductsPageComponent,
    ProductCategoriesListComponent,
    ProductsTableComponent,
  ],
})
export class ProductsModule {}
