import { Component, Input } from '@angular/core';
import { Product } from '../data/product';

@Component({
  selector: 'hai-products-table',
  template: `<table mat-table [dataSource]="products" class="w-full">
    <!-- Id Column -->
    <ng-container matColumnDef="name">
      <th mat-header-cell *matHeaderCellDef>Name</th>
      <td mat-cell *matCellDef="let element">{{ element.name }}</td>
    </ng-container>

    <!-- Customer Name Column -->
    <ng-container matColumnDef="price">
      <th mat-header-cell *matHeaderCellDef>Price</th>
      <td mat-cell *matCellDef="let element">{{ element.price }}</td>
    </ng-container>

    <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
    <tr mat-row *matRowDef="let row; columns: displayedColumns"></tr>
  </table>`,
})
export class ProductsTableComponent {
  @Input() products: Product[] = [];

  displayedColumns: string[] = ['name', 'price'];
}
