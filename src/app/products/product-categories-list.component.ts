import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProductCategory } from '../data/product-category';

@Component({
  selector: 'hai-product-categories-list',
  template: `
    <mat-selection-list [multiple]="false">
      <mat-list-option
        *ngFor="let pc of productCategories"
        (click)="productCategorySelected.emit(pc.id)"
        >{{ pc.name }}</mat-list-option
      >
    </mat-selection-list>
  `,
})
export class ProductCategoriesListComponent {
  @Output() productCategorySelected = new EventEmitter<number>();

  @Input() productCategories: ProductCategory[] = [];
}
