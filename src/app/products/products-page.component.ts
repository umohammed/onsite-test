import { Component } from '@angular/core';
import { ProductsPageService } from './products-page.service';

@Component({
  template: `<hai-page-title title="Products"></hai-page-title>
    <div class="flex">
      <hai-product-categories-list
        *ngIf="productCategories$ | async as pc"
        [productCategories]="pc"
        (productCategorySelected)="onProductCategorySelected($event)"
        class="border-2 p-4"
      ></hai-product-categories-list>
      <hai-products-table
        *ngIf="products$ | async as products"
        [products]="products"
        class="flex-grow ml-3"
      ></hai-products-table>
    </div> `,
  providers: [ProductsPageService],
})
export class ProductsPageComponent {
  productCategories$ = this.productsPageService.productCategories$;
  products$ = this.productsPageService.filteredProducts$;

  constructor(private productsPageService: ProductsPageService) {}

  onProductCategorySelected(id: number) {
    this.productsPageService.productCategorySelected(id);
  }
}
