export interface ProductCategory {
  id: number;
  name: string;
}

export function createProductCategoryData(): ProductCategory[] {
  return [
    { id: 1, name: 'Beverages' },
    { id: 2, name: 'Grains/Cereals' },
    { id: 3, name: 'Dairy Products' },
    { id: 4, name: 'Confections' },
    { id: 5, name: 'Seafood' },
  ];
}
