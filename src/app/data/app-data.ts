import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Customer, createCustomerData } from './customer';
import { createOrderData, Order } from './order';
import { createProductData, Product } from './product';
import { createProductCategoryData, ProductCategory } from './product-category';

export class AppData implements InMemoryDbService {
  createDb(): {
    products: Product[];
    orders: Order[];
    customers: Customer[];
    productCategories: ProductCategory[];
  } {
    return {
      customers: createCustomerData(),
      orders: createOrderData(),
      products: createProductData(),
      productCategories: createProductCategoryData(),
    };
  }
}
