import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../data/product';
import { Customer } from './customer';
import { Order } from './order';
import { ProductCategory } from './product-category';

@Injectable({ providedIn: 'root' })
export class ApiService {
  products$ = this.http.get<Product[]>('api/products');
  customers$ = this.http.get<Customer[]>('api/customers');
  orders$ = this.http.get<Order[]>('api/orders');
  productCategories$ = this.http.get<ProductCategory[]>(
    'api/productCategories'
  );

  constructor(private http: HttpClient) {}
}
