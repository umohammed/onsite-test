export interface Product {
  id: number;
  name: string;
  price: number;
  productCategoryId: number;
}

export function createProductData(): Product[] {
  return [
    { id: 1, name: 'Filo Mix', price: 7, productCategoryId: 2 },
    { id: 2, name: 'Camembert Pierrot', price: 34, productCategoryId: 3 },
    { id: 3, name: 'Mascarpone Fabioli', price: 32, productCategoryId: 3 },
    { id: 4, name: 'Röd Kaviar', price: 15, productCategoryId: 5 },
    { id: 5, name: 'Chocolade', price: 12.75, productCategoryId: 4 },
    { id: 6, name: 'Ipoh Coffee', price: 46, productCategoryId: 1 },
  ];
}
