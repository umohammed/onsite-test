import { Component, Input } from '@angular/core';

@Component({
  selector: 'hai-page-title',
  template: `<h1 class="text-3xl mb-2">{{ title }}</h1>`,
})
export class PageTitleComponent {
  @Input() title = '';
}
