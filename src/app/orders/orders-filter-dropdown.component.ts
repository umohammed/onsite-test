import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { Customer } from '../data/customer';

@Component({
  selector: 'hai-orders-filter-drop-down',
  template: `<mat-form-field appearance="fill">
    <mat-label>Customer</mat-label>
    <mat-select (selectionChange)="onChange($event)">
      <mat-option></mat-option>
      <mat-option *ngFor="let customer of customers" [value]="customer.id">
        {{ customer.name }}
      </mat-option>
    </mat-select>
  </mat-form-field> `,
})
export class OrdersFilterDropDown {
  @Input() customers: Customer[] | null = null;

  @Output() selectionChanged: EventEmitter<number> = new EventEmitter();

  onChange({ value }: MatSelectChange) {
    this.selectionChanged.emit(+value);
  }
}
