import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { RouterModule, Routes } from '@angular/router';
import { ShellComponent } from '../core/shell.component';
import { SharedModule } from '../shared/shared.module';
import { OrdersFilterDropDown } from './orders-filter-dropdown.component';
import { OrdersPageComponent } from './orders-page.component';
import { OrdersTableComponent } from './orders-table.component';

const routes: Routes = [
  {
    path: '',
    component: ShellComponent,
    children: [{ path: '', component: OrdersPageComponent }],
  },
];

@NgModule({
  imports: [
    CommonModule,
    MatSelectModule,
    MatTableModule,
    RouterModule.forChild(routes),
    SharedModule,
  ],
  declarations: [
    OrdersPageComponent,
    OrdersFilterDropDown,
    OrdersTableComponent,
  ],
})
export class OrdersModule {}
