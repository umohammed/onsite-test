import { Component } from '@angular/core';
import { OrdersPageService } from './orders-page.service';

@Component({
  selector: 'hai-orders-page',
  template: `
    <hai-page-title title="Orders"></hai-page-title>
    <div class="flex items-baseline">
      <hai-orders-filter-drop-down
        [customers]="customers$ | async"
        (selectionChanged)="ordersService.selectedCustomerChanged($event)"
      ></hai-orders-filter-drop-down>
      <div class="ml-auto">{{ loadedTime$ | async | date: 'long' }}</div>
    </div>
    <hai-orders-table
      *ngIf="orders$ | async as orders"
      [orders]="orders"
    ></hai-orders-table>
  `,
  providers: [OrdersPageService],
})
export class OrdersPageComponent {
  orders$ = this.ordersService.filteredOrders$;
  customers$ = this.ordersService.customers$;
  loadedTime$ = this.ordersService.loadedTime$;

  constructor(public ordersService: OrdersPageService) {}
}
