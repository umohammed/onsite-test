import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, timer } from 'rxjs';
import { ApiService } from '../data/api.service';
import { map, shareReplay, switchMap } from 'rxjs/operators';
import {
  filterOrderByCustomer,
  getOrderListViewModels,
  Order,
} from '../data/order';
import { Customer } from '../data/customer';
import { Product } from '../data/product';

@Injectable()
export class OrdersPageService {
  private selectedCustomerIdSubject = new BehaviorSubject<number | null>(null);

  private data$ = timer(0, 5000).pipe(
    switchMap(() =>
      combineLatest([
        this.apiService.customers$,
        this.apiService.orders$,
        this.apiService.products$,
      ]).pipe(map((data) => ({ loadedTime: Date.now(), data })))
    ),
    shareReplay(1)
  );

  private orderListViewModels$ = this.data$.pipe(
    map(({ data }) => this.getOrderListViewModels(data))
  );

  loadedTime$ = this.data$.pipe(map(({ loadedTime }) => loadedTime));

  customers$ = this.data$.pipe(map(({ data: [customers] }) => customers));

  filteredOrders$ = this.orderListViewModels$.pipe(
    switchMap((orders) =>
      this.selectedCustomerIdSubject.pipe(
        map((customerId) =>
          customerId ? orders.filter(filterOrderByCustomer(customerId)) : orders
        )
      )
    )
  );

  constructor(private apiService: ApiService) {}

  selectedCustomerChanged(customerId: number) {
    this.selectedCustomerIdSubject.next(customerId);
  }

  private getOrderListViewModels([customers, orders, products]: [
    Customer[],
    Order[],
    Product[]
  ]) {
    return getOrderListViewModels({ customers, orders, products });
  }
}
