import { Component, Input } from '@angular/core';
import { OrderListViewModel } from '../data/order';

@Component({
  selector: `hai-orders-table`,
  template: `
    <table mat-table [dataSource]="orders" class="w-full">
      <!-- Id Column -->
      <ng-container matColumnDef="id">
        <th mat-header-cell *matHeaderCellDef>Order Id</th>
        <td mat-cell *matCellDef="let element">{{ element.id }}</td>
      </ng-container>

      <!-- Customer Name Column -->
      <ng-container matColumnDef="customerName">
        <th mat-header-cell *matHeaderCellDef>Customer Name</th>
        <td mat-cell *matCellDef="let element">{{ element.customerName }}</td>
      </ng-container>

      <!-- Date Column -->
      <ng-container matColumnDef="date">
        <th mat-header-cell *matHeaderCellDef>Date</th>
        <td mat-cell *matCellDef="let element">
          {{ element.date | date: 'dd/MM/yyyy' }}
        </td>
      </ng-container>

      <!-- PRoduct Name Column -->
      <ng-container matColumnDef="productName">
        <th mat-header-cell *matHeaderCellDef>Product Name</th>
        <td mat-cell *matCellDef="let element">{{ element.productName }}</td>
      </ng-container>

      <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
      <tr mat-row *matRowDef="let row; columns: displayedColumns"></tr>
    </table>
  `,
})
export class OrdersTableComponent {
  @Input() orders: OrderListViewModel[] = [];

  displayedColumns: string[] = ['id', 'customerName', 'date', 'productName'];
}
