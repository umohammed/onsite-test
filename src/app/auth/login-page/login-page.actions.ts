import { createAction, props } from '@ngrx/store';
import { Credentials } from '../credentials';

export const LoginPageActions = {
  loginClicked: createAction(
    '[Login Page] Login Clicked',
    props<{ credentials: Credentials }>()
  ),
};
