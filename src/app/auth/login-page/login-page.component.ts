import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { selectAuthError } from '../store/auth.reducer';
import { LoginPageActions } from './login-page.actions';

@Component({
  selector: 'hai-login-page',
  template: `
    <form [formGroup]="loginForm" (ngSubmit)="onSubmit()">
      <div class="flex flex-col w-96 p-4 bg-white">
        <div
          class="p-2 mb-3 bg-red-300 border-2 text-red-900 border-red-900 rounded-md"
          [class.invisible]="!(loginError$ | async)"
        >
          Invalid username or password
        </div>
        <mat-form-field appearance="fill">
          <mat-label>Email</mat-label>
          <input matInput formControlName="email" />
          <mat-error *ngIf="loginForm.controls.email.invalid"
            >A valid email is required</mat-error
          >
        </mat-form-field>
        <mat-form-field appearance="fill">
          <mat-label>Password</mat-label>
          <input type="password" matInput formControlName="password" />
          <mat-error *ngIf="loginForm.controls.password.invalid"
            >A password is required</mat-error
          >
        </mat-form-field>
        <div class="mt-2">
          <button mat-button>Login</button>
        </div>
      </div>
    </form>
  `,
  host: { class: 'grid items-center justify-center h-full' },
})
export class LoginComponent {
  loginForm = this.fb.group({
    email: [
      'test@example.com',
      Validators.compose([Validators.required, Validators.email]),
    ],
    password: ['Qwerty123', Validators.required],
  });

  loginError$ = this.store.select(selectAuthError);

  constructor(private store: Store, private fb: FormBuilder) {}

  onSubmit() {
    if (this.loginForm.valid) {
      this.store.dispatch(
        LoginPageActions.loginClicked({ credentials: this.loginForm.value })
      );
    }
  }
}
