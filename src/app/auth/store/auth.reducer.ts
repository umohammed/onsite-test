import {
  Action,
  createFeatureSelector,
  createReducer,
  createSelector,
  on,
} from '@ngrx/store';
import { AuthApiActions } from '../api/auth-api.actions';
import { LoginPageActions } from '../login-page/login-page.actions';

export const authFeatureKey = 'auth';

export interface State {
  email: string | null;
  error: string | null;
}

export const initialState: State = {
  error: null,
  email: null,
};

const authReducer = createReducer(
  initialState,
  on(LoginPageActions.loginClicked, (state) => ({
    ...state,
    error: null,
  })),
  on(AuthApiActions.loginSuccess, (state, { email }) => ({
    ...state,
    email,
  })),
  on(AuthApiActions.loginFailure, (state, { error }) => ({
    ...state,
    error,
  }))
);

const getEmail = (state: State) => state?.email || null;
const getError = (state: State) => state?.error || null;

export function reducer(state: State | undefined, action: Action) {
  return authReducer(state, action);
}

export const selectAuthState = createFeatureSelector<State>(authFeatureKey);

export const selectAuthError = createSelector(selectAuthState, getError);

export const selectAuthEmail = createSelector(selectAuthState, getEmail);
