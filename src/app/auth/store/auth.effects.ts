import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';
import { AuthApiActions } from '../api/auth-api.actions';
import { AuthApiService } from '../api/auth-api.service';
import { LoginPageActions } from '../login-page/login-page.actions';

@Injectable()
export class AuthEffects {
  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LoginPageActions.loginClicked),
      exhaustMap(({ credentials }) =>
        this.authService.login(credentials).pipe(
          map((email) => AuthApiActions.loginSuccess({ email })),
          catchError((error) => of(AuthApiActions.loginFailure({ error })))
        )
      )
    )
  );

  loginSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthApiActions.loginSuccess),
        tap(() => this.router.navigate(['/']))
      ),
    {
      dispatch: false,
    }
  );

  constructor(
    private actions$: Actions,
    private authService: AuthApiService,
    private router: Router
  ) {}
}
