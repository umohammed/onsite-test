import { createAction, props } from '@ngrx/store';

export const AuthApiActions = {
  loginSuccess: createAction(
    '[Auth API] User Login Success',
    props<{ email: string }>()
  ),
  loginFailure: createAction(
    '[Auth API] User Login Failed',
    props<{ error: string }>()
  ),
};
