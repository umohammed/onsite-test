import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { Credentials, validateCredentials } from '../credentials';

@Injectable({ providedIn: 'root' })
export class AuthApiService {
  login({ email, password }: Credentials): Observable<string> {
    return validateCredentials({ email, password })
      ? of(email)
      : throwError('Invalid username or password');
  }
}
