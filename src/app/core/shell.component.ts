import { Component } from '@angular/core';

@Component({
  selector: 'hai-shell',
  template: `<hai-nav-toolbar></hai-nav-toolbar>
    <div class="bg-white flex-grow m-24 p-24">
      <router-outlet></router-outlet>
    </div> `,
  host: { class: 'h-full flex flex-col' },
})
export class ShellComponent {}
