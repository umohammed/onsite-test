import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NotFoundPageComponent } from './not-found-page.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ShellComponent } from './shell.component';
import { NavToolbarComponent } from './nav-toolbar.component';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [NotFoundPageComponent, ShellComponent, NavToolbarComponent],
  imports: [RouterModule, MatToolbarModule, MatButtonModule],
})
export class CoreModule {}
