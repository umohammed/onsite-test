import { Component } from '@angular/core';

@Component({
  selector: `hai-not-found-page`,
  template: `<div>Page not found</div>

    <a routerLink="/">Home</a> `,
})
export class NotFoundPageComponent {}
