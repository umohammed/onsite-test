import { Component } from '@angular/core';

@Component({
  selector: 'hai-nav-toolbar',
  template: `<mat-toolbar>
    <a routerLinkActive="bg-gray-200" mat-button routerLink="/orders">Orders</a>
    <a routerLinkActive="bg-gray-200" mat-button routerLink="/products"
      >Products</a
    >
    <div class="flex-grow"></div>
    <a mat-button routerLink="/login">Logout</a>
  </mat-toolbar>`,
})
export class NavToolbarComponent {}
